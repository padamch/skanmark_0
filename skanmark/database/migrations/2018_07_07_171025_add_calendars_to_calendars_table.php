<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalendarsToCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('calendars')->insert(array(
            'date'=>'2018-07-28',
            'timeoption' => 3,
            'user_id'=>3,
            'category_id'=>1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('calendars')->insert(array(
            'date'=>'2018-07-29',
            'timeoption' => 2,
            'user_id'=>4,
            'category_id'=>1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('calendars')->insert(array(
            'date'=>'2018-07-30',
            'timeoption' => 2,
            'user_id'=>5,
            'category_id'=>1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('calendars')->where('id','=',1)->delete();
    }
}