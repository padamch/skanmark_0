<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Admins
        DB::table('users')->insert(array(
            'name' => 'Ad. Anders Jensen',
            'email' => 'admin@admin.dk',
            'password' => Hash::make('123456'),
            'role_id' => 1, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Ad. Bent Nielsen',
            'email' => 'admin1@admin.dk',
            'password' => Hash::make('123456'),
            'role_id' => 1, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        //Engineers
        DB::table('users')->insert(array(
            'name' => 'Eng. Claus Jensen',
            'email' => 'eng@eng.dk',
            'password' => Hash::make('123456'),
            'role_id' => 2, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Eng. Martin Nielsen',
            'email' => 'eng1@eng.dk',
            'password' => Hash::make('123456'),
            'role_id' => 2, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Eng. Emil Poulsen',
            'email' => 'eng2@eng.dk',
            'password' => Hash::make('123456'),
            'role_id' => 2, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Eng. Erik Thomsen',
            'email' => 'eng3@eng.dk',
            'password' => Hash::make('123456'),
            'role_id' => 2, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        //Engineers
        DB::table('users')->insert(array(
            'name' => 'Eng. Mads Pedersen',
            'email' => 'eng4@eng.dk',
            'password' => Hash::make('123456'),
            'role_id' => 2, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Eng. Anne Larsen',
            'email' => 'eng5@eng.dk',
            'password' => Hash::make('123456'),
            'role_id' => 2, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Eng. Morten Olsen',
            'email' => 'eng6@eng.dk',
            'password' => Hash::make('123456'),
            'role_id' => 2, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        //Users
        DB::table('users')->insert(array(
            'name' => 'Us. Claus Jensen',
            'email' => 'us@us.dk',
            'password' => Hash::make('123456'),
            'role_id' => 3, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Us. Martin Nielsen',
            'email' => 'us1@us.dk',
            'password' => Hash::make('123456'),
            'role_id' => 3, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Us. Emil Poulsen',
            'email' => 'us2@us.dk',
            'password' => Hash::make('123456'),
            'role_id' => 3, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Uss. Erik Thomsen',
            'email' => 'us3@us.dk',
            'password' =>Hash::make('123456'),
            'role_id' => 3, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));      
        DB::table('users')->insert(array(
            'name' => 'Us. Mads Pedersen',
            'email' => 'us4@us.dk',
            'password' => Hash::make('123456'),
            'role_id' => 3, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Us. Anne Larsen',
            'email' => 'us5@us.dk',
            'password' => Hash::make('123456'),
            'role_id' => 3, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('users')->insert(array(
            'name' => 'Us. Morten Olsen',
            'email' => 'us6@us.dk',
            'password' => Hash::make('123456'),
            'role_id' => 3, 
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('users')->where('email','=','admin@admin.dk')->delete();
        DB::table('users')->where('email','=','admin1@admin.dk')->delete();
    }
}
