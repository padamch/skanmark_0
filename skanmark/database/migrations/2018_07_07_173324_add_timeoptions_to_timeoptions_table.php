<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeoptionsToTimeoptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //id:1
        DB::table('timeoptions')->insert(array(
            'start_time'=>'08:00',
            'end_time'=>'09:00',
            'available'=>1,
            'calendar_id'=>1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('timeoptions')->insert(array(
            'start_time'=>'09:30',
            'end_time'=>'10:00',
            'available'=>1,
            'calendar_id'=>1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('timeoptions')->insert(array(
            'start_time'=>'11:00',
            'end_time'=>'12:00',
            'available'=>1,
            'calendar_id'=>1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        //id:2
        DB::table('timeoptions')->insert(array(
            'start_time'=>'15:00',
            'end_time'=>'16:00',
            'available'=>1,
            'calendar_id'=>2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('timeoptions')->insert(array(
            'start_time'=>'13:00',
            'end_time'=>'14:00',
            'available'=>1,
            'calendar_id'=>2,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        //id:3
        DB::table('timeoptions')->insert(array(
            'start_time'=>'13:00',
            'end_time'=>'14:00',
            'available'=>1,
            'calendar_id'=>3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
        DB::table('timeoptions')->insert(array(
            'start_time'=>'10:00',
            'end_time'=>'11:00',
            'available'=>1,
            'calendar_id'=>3,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('timeoptions')->where('id','=',1)->delete();
    }
}
