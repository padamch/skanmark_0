<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableIfExists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //drop database skanmark_0;create database skanmark_0;
        //Schema::dropIfExists('services');
        //Schema::dropIfExists('AddRolesToRolesTable');
        //Schema::dropIfExists('AddUsersToUsersTable');
        //
        //Schema::dropIfExists('AddCategoriesToCategoriesTable');
        Schema::dropIfExists('CreateTimeoptionsTable');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
