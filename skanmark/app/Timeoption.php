<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timeoption extends Model
{
    //
    protected $fillable = [
        'start_time','end_time','available','calendar_id','user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function categories(){
        return $this->belongsTo('App\Category');
    }

    public function calendar(){
        return $this->belongsTo('App\Calendar');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }
}
