<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    //

    protected $fillable = [
        'date','timeoption','user_id','category_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function categories(){
        return $this->belongsTo('App\Calendar');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment');
    }

    public function timeoptions(){
        return $this->hasMany('App\Timeoption');
    }
}
