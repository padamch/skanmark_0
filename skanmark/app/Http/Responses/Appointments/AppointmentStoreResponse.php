<?php

namespace App\Http\Responses\Appointments;


use Illuminate\Contracts\Support\Responsable;
use App\Appointment;
use App\Calendar;
use App\Timeoption;
use Auth;

class AppointmentStoreResponse implements Responsable
{
    
    public function toResponse($request)
    {
    	
        $timeoption_id = $request['timeoption_id'];
        $calendar_id = Timeoption::find($timeoption_id)->calendar->id;
        $user_id = Auth::user()->id;

    	$appointments = Appointment::create([
            'message' => $request['message'],
            'user_id' => $user_id,
            'calendar_id' => $calendar_id,
            'timeoption_id' => $timeoption_id,
        ]);
        if($appointments)
        {
            //update timeoption available
            $minusAvailable = Timeoption::where('id',$timeoption_id)
                            ->decrement('available');
            if($minusAvailable)
            {
                //update calendar timeoption field
                $minusTimeoption = Calendar::where('id',$calendar_id)
                                ->decrement('timeoption');
                if($minusTimeoption)
                {
                  return back()->with('success','Appointment booked successfully!');  
                }
            }
            
        }
    }
}