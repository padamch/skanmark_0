<?php

namespace App\Http\Responses\Appointments;


use Illuminate\Contracts\Support\Responsable;
use App\Timeoption;
use Auth;

class AppointmentTimeoptionResponse implements Responsable
{
    public function toResponse($request)
    {
        $data = $request->all();
        $calendar_id = $data['calendar_id'];
        $timeoptions = Timeoption::where('calendar_id',$calendar_id)
        				->where('available',1)->get();

        //print_r($timeoptions->toJson());
        
        if($request->ajax())
        {
           return view('appointments.timeoptions.index_ajax')->with('timeoptions',$timeoptions);
        }
        
        return view('appointments.timeoptions.index')->with('timeoptions',$timeoptions);
        
    }
}