<?php

namespace App\Http\Responses\Appointments;


use Illuminate\Contracts\Support\Responsable;
use App\Calendar;
use Auth;

class AppointmentIndexResponse implements Responsable
{
    public function toResponse($request)
    {
        $m = date('m');//current month
        $calendars = Calendar::select('id','date')
        			->whereMonth('date',$m)
                    ->where('timeoption','>',0)
                    ->where('category_id','=',1)
                    ->get();
        return view('appointments.index')->with('calendars',$calendars);
        //print_r($calendars->toJson());
    }
}