<?php

namespace App\Http\Responses\Timeoptions;


use Illuminate\Contracts\Support\Responsable;
use App\Timeoption;

class TimeoptionCreateResponse implements Responsable
{
    public function toResponse($request)
    {
        //dd($request->date);
        return view('timeoptions.create')->with('calendar',$request);
    }
}