<?php

namespace App\Http\Responses\Timeoptions;


use Illuminate\Contracts\Support\Responsable;
use App\Calendar;
use Auth;

class TimeoptionIndexResponse implements Responsable
{
    public function toResponse($request)
    {
        $user_id = Auth::user()->id;
        $dates = Calendar::where('user_id',$user_id)->get();
       // return view('timeoptions.index')->with('dates',$dates);
        print_r($dates->toJson());
    }
}