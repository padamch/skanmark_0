<?php

namespace App\Http\Responses\Timeoptions;


use Illuminate\Contracts\Support\Responsable;
use App\Timeoption;
use App\Calendar;
use Auth;

class TimeoptionStoreResponse implements Responsable
{
    
    public function toResponse($request)
    {
    	$calendar_id = $request['calendar_id'];
        $user_id = Auth::user()->id;
    	$timeoptions = Timeoption::create([
            'start_time' => $request['start_time'],
            'end_time' => $request['end_time'],
            'calendar_id' => $calendar_id,
            'user_id' => $user_id,
        ]);
        if($timeoptions)
        {
            //update calendar timeoption field
            $addTimeoption = Calendar::where('id',$calendar_id)
                            ->increment('timeoption');
            if($addTimeoption)
            {
              return back()->with('success','Timeoption added successfully!');  
            }
        }
        
    }
}