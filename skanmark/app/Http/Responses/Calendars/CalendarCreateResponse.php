<?php

namespace App\Http\Responses\Calendars;


use Illuminate\Contracts\Support\Responsable;
use App\Category;

class CalendarCreateResponse implements Responsable
{
    public function toResponse($request)
    {
         
    	$categories= Category::all();
        return view('calendars.create')->with('categories',$categories);
    }
}