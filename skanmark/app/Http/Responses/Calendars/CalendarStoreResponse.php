<?php

namespace App\Http\Responses\Calendars;


use Illuminate\Contracts\Support\Responsable;
use App\Calendar;
use Auth;

class CalendarStoreResponse implements Responsable
{
    
    public function toResponse($request)
    {
    	if(Auth::check()){
            $calendar=Calendar::create([
                'date'=> $request->input('date'),
                'user_id'=> Auth::user()->id,
                'category_id'=> $request->input('category_id')
            ]);
            if($calendar){
                return redirect()->route('calendars.index', ['calendar_id'=> $calendar->id])->with('success', 'Calendar created successfully');
            }
        }

        return back()->withInput()->with('errors','Error creating new calendar');
        
    }
}