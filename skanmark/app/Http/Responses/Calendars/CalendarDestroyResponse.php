<?php

namespace App\Http\Responses\Calendars;


use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Collection;
use App\Calendar;
use Auth;

class CalendarDestroyResponse implements Responsable
{
    protected $calendars;

    public function __construct(Calendar $calendars)
    {
        $this->calendars = $calendars;
    }

    public function toResponse($request)
    {
    	//print_r($this->calendars->all()->toJson());
        $r = $this->calendars->all();
        echo $r->date;
        /*
        $findDate = Calendar::find($request->calendar_id);
        if($findDate->delete()){
            return redirect()->route('calendars.index')->with('success','Calendar date deleted successfully!');
        }
        return back()->withInput()->with('errors','Calendar Date could not be deleted');
        */
        
    }
}