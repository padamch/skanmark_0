<?php

namespace App\Http\Responses\Calendars;


use Illuminate\Contracts\Support\Responsable;
use App\Calendar;
use Auth;

class CalendarIndexResponse implements Responsable
{
    public function toResponse($request)
    {
        $user_id = Auth::user()->id;
        $dates = Calendar::where('user_id',$user_id)->get();
        return view('calendars.index')->with('dates',$dates);
        //print_r($dates->toJson());
    }
}