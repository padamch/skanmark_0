<?php

namespace App\Http\Responses\Engineers;


use Illuminate\Contracts\Support\Responsable;
use App\User;

class EngineerShowResponse implements Responsable
{
    
	protected $engineer;

	public function __construct(User $engineer){
		$this->engineer = $engineer;
	}
    
    public function toResponse($request)
    {
        $engineer = User::find($this->engineer->id);
        return view('engineers.show', ['engineer'=>$engineer]);
    }
}