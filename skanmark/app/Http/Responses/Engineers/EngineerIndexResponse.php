<?php

namespace App\Http\Responses\Engineers;


use Illuminate\Contracts\Support\Responsable;
use App\Engineer;
use App\User;
use Auth;

class EngineerIndexResponse implements Responsable
{
    public function toResponse($request)
    {
        //TODO: check if admin is logged in
        $engineers = User::where('role_id','=','2')->orderBy('name', 'desc')->get();
        return view('engineers.index')->with('engineers',$engineers);
    }
}