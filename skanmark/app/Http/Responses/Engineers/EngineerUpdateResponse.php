<?php

namespace App\Http\Responses\Engineers;


use Illuminate\Contracts\Support\Responsable;
use App\User;

class EngineerUpdateResponse implements Responsable
{
    
	protected $engineer;

	public function __construct(User $engineer){
		$this->engineer = $engineer;
	}
    
    public function toResponse($request)
    {
        $engineerUpdate = User::where('id', $this->engineer->id)->update([
                'name'=> $request->input('name')/*,
                'address'=> $request->input('address'),
                'phone_number'=> $request->input('phone_number')*/
            ]);

        if($engineerUpdate){
            return redirect()->route('engineers.show',['engineer'=>$this->engineer->id])->with('success', 'Engineer updated successfully!');
        }
        //return
        return back()->withInput();
    }
}