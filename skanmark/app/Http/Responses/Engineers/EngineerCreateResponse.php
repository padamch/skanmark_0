<?php

namespace App\Http\Responses\Engineers;


use Illuminate\Contracts\Support\Responsable;

class EngineerCreateResponse implements Responsable
{
    public function toResponse($request)
    {
        return view('engineers.create');
    }
}