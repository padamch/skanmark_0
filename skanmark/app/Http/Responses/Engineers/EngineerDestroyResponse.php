<?php

namespace App\Http\Responses\Engineers;


use Illuminate\Contracts\Support\Responsable;
use App\User;

class EngineerDestroyResponse implements Responsable
{
    
	protected $engineer;

	public function __construct(User $engineer){
		$this->engineer = $engineer;
	}
    
    public function toResponse($request)
    {
        $findengineer = User::find($this->engineer->id);
        if($findengineer->delete()){
            return redirect()->route('engineers.index')->with('success','Engineer deleted successfully!');
        }
        return back()->withInput()->with('errors','Engineer could not be deleted');
    }
}