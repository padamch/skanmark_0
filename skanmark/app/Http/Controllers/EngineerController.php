<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Http\Responses\Engineers\EngineerIndexResponse;
use App\Http\Responses\Engineers\EngineerCreateResponse;
use App\Http\Responses\Engineers\EngineerShowResponse;
use App\Http\Responses\Engineers\EngineerEditResponse;
use App\Http\Responses\Engineers\EngineerUpdateResponse;
use App\Http\Responses\Engineers\EngineerDestroyResponse;
use Illuminate\Http\Request;
use App\User;


class EngineerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new EngineerIndexResponse();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return new EngineerCreateResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function show(User $engineer)
    {
        //
        return new EngineerShowResponse($engineer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function edit(User $engineer)
    {
        //
        return new EngineerEditResponse($engineer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $engineer)
    {
        //
        return new EngineerUpdateResponse($engineer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Engineer  $engineer
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $engineer)
    {
        //
        return new EngineerDestroyresponse($engineer);
    }
}