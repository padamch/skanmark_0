<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Responses\Timeoptions\TimeoptionIndexResponse;
use App\Http\Responses\Timeoptions\TimeoptionCreateResponse;
use App\Http\Responses\Timeoptions\TimeoptionStoreResponse;
//use App\Http\Responses\Timeoptions\TimeoptionShowResponse;
use App\Timeoption;




class TimeoptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return new TimeoptionIndexResponse();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return new TimeoptionCreateResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return new TimeoptionStoreResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Timeoption  $timeoption
     * @return \Illuminate\Http\Response
     */
    public function show(Timeoption $timeoption)
    {
        //
        dd($timeoption->id);
        //return new TimeoptionShowResponse();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Timeoption  $timeoption
     * @return \Illuminate\Http\Response
     */
    public function edit(Timeoption $timeoption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Timeoption  $timeoption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timeoption $timeoption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Timeoption  $timeoption
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timeoption $timeoption)
    {
        //
    }
}