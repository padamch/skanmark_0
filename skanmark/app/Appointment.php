<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    protected $fillable = [
        'message','user_id','calendar_id','timeoption_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function calendars(){
        return $this->belongsTo('App\Calendar');
    }

    public function timeoptions(){
        return $this->belongsTo('App\Timeoption');
    }
}
