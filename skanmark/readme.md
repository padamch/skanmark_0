# skanmark

How to run

### Steps ###
* C:\project>```git clone url.git```
* Go to project folder 'skanmark'
* C:\skanmark_0\skanmark>```composer install```
* C:\skanmark_0\skanmark>```php artisan key:generate```
* C:\skanmark_0\skanmark>```php artisan package:discover```
* create database 'skanmark'
* Rename '.env.example' to .env and update database credentials  
* run C:\skanmark_0\skanmark>```php artisan serve```
* run C:\skanmark_0\skanmark>```php artisan migrate``` or #```php artisan migrate:refresh``` to start over
* open browser localhost:8000
* login credentials: username:admin@admin.dk password:123456 or you can see the credential at migration table 'add_users_to_users_table.php'