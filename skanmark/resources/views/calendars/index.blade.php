@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
       @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
      <ul class="list-group">
          <div class="list-group-item d-flex justify-content-between align-items-center">Click date to add Timeoption<span><a href="calendars/create">+ CREATE NEW DATE</a></span></div>
          @foreach ($dates as $date)
            <li class="list-group-item">
                <a href="{{ route('timeoptions.create', ['calendar_id'=> $date->id, 'date'=> $date->date]) }}">{{$date->date}}</a>
                <span class="float-right">
                  <a href="calendars/{{$date->id}}/edit" class="btn btn-outline-secondary btn-sm" style="padding:1px 1px; margin-right:10px;">&#9998; Edit</a><a href="#" class="btn btn-outline-danger btn-sm" style="padding:1px 1px;" onclick="deleteDate()">&#10008; Delete</a>
                </span>
              <form id="delete-form" action="{{ route('calendars.destroy', [$date->id]) }}" method ="POST" style="display: none;">
                <input type="hidden" name="_method" value="delete">
              {{csrf_field()}}
              </form>
            </li>
          @endforeach
      </ul>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript">
  function deleteDate(){
    var result=confirm('Are you sure you wish to delete this item?');
    if(result){
        event.preventDefault();
        document.getElementById('delete-form').submit();
    }
  }
</script>