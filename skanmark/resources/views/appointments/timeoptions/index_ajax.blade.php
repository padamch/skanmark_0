<form  method="POST" action="{{ Route('appointments.store') }}">
	@csrf
	<div>Select Time</div>
	@foreach($timeoptions as $timeoption)
    <div class="radio">
      <label><input type="radio" name="timeoption_id" value="{{$timeoption->id}}" required> {{ \Carbon\Carbon::parse($timeoption->start_time)->format('H:i') }} - {{\Carbon\Carbon::parse($timeoption->end_time)->format('H:i')}}</label>
    </div>
    @endforeach
    <div class="form-group row">
	    <div class="col-md-6">
	    	<textarea rows="4" cols="50" name="message" class="form-control">Enter message here...</textarea>
		</div>
	</div>

	<div class="form-group row">
            <button type="submit" class="btn btn-primary">
                {{ __('Book') }}
            </button>
        </div>
</form>