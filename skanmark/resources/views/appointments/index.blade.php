@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
       <div class="card">
            <div class="card-header">{{ __('Book Appointment') }}
                <a href="/calendars" style="float:right;">+ Create Appointment</a>
            </div>

            <div class="card-body">
                <div class="form-group row">
                    <div id="datepicker"></div>
                </div>
                <div id="timeoption"></div>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript">
    var calendars = @json($calendars);
    var l = Object.keys(calendars).length; //Length of object
    var availableDates = [];
    for(i=0; i<l;i++)
    {
        availableDates[i] = calendars[i]['date'];
    }
    //console.log(availableDates);
</script>