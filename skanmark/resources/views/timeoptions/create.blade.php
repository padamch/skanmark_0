@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Create Timeoptions') }}<a href="/calendars" style="float:right;">&lArr; Back</a></div>

                <div class="card-body">
                    <form method="POST" action="{{ Route('timeoptions.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Selected Date') }}</label>
                            <div class="col-md-6">
                                <input type="text" name="date" value="{{$calendar->date}}" class="form-control" readonly>
                                <input type="hidden" name="calendar_id" value="{{$calendar->calendar_id}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="start_time" class="col-md-4 col-form-label text-md-right">{{ __('Start Time') }}</label>
                            <div class="col-md-6">
                                <input type="time" class="form-control" name="start_time" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="end_time" class="col-md-4 col-form-label text-md-right">{{ __('Expected End Time') }}</label>
                            <div class="col-md-6">
                                <input type="time" class="form-control" name="end_time" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
