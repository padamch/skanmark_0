@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
         <table class="table">
            <thead>
              <tr>
                <th scope="col">Engineer detail</th>
                <th scope="col"><a href="/engineers">&lArr; Back</a></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">Name</th>
                <td>{{$engineer->name}}</td>
              </tr>
              <tr>
                <th scope="row">Email</th>
                <td>{{$engineer->email}}</td>
              </tr>
              <tr>
                <th scope="row">Created at</th>
                <td>{{$engineer->created_at}}</td>
              </tr>
              <tr>
                <th scope="row">Updated at</th>
                <td>{{$engineer->updated_at}}</td>
              </tr>
            </tbody>
          </table>
      </div>
  </div>
</div>
@endsection