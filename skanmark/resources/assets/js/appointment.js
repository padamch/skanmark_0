$(function(){
	var root_url = 'http://localhost:8000/';

	var calendar_id = 0;
	$('#datepicker').datepicker({
		dateFormat: 'yy-mm-dd',
	    beforeShowDay: enableDays,
	    onSelect: function(date, obj){
	    	//console.log(date);
	    	var item = calendars.find(item => item.date === date); //to find the item
	    	calendar_id = item.id;
	    	//console.log(calendar_id);
	    	$.ajax({
	            type: 'GET', 
	            url :root_url + 'appointments/timeoptions',
	            data: {calendar_id : calendar_id},
	            success : function (data) {
	                //console.log(data);
	                $("#timeoption").html(data);
	            }
    		});//end ajax
	    }
	});
	
	// enable these days
	function enableDays(date) {
        var sdate = $.datepicker.formatDate( 'yy-mm-dd', date)
        if($.inArray(sdate, availableDates) != -1) {
            return [true];
        }
        return [false];
    }
});//end function