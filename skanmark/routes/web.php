<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('engineers', 'EngineerController');
Route::resource('categories', 'CategoryController');
Route::resource('calendars', 'CalendarController');
Route::resource('timeoptions', 'TimeoptionController');

Route::get('/appointments/timeoptions','AppointmentController@timeoption')->name('appointments.timeoption'); //it should be before appointments
Route::resource('appointments', 'AppointmentController');


//only admin can access
Route::middleware(['admin'])->group(function () {           
   Route::resource('roles', 'RoleController');
});