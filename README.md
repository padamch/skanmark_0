# skanmark


### Skanmark - appointment booking Laravel App ###


* Go to 'Skanmark' folder to check how to run!

* If you want to rename the migration file name, 1. go to table 'drop_table_if_exists' and drop that table. Then, php artisan migrate:fresh. It will not delete the file but delete the class name internally. 2. Now create the table with whatever name with php artisan make:migration and rename it before you run migration. When everything is  okay, Run 3. php artisan migrate:fresh